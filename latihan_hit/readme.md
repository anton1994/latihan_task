there are three arguments

### --celery_worker ###
> python -m latihan_hit --celery_worker

this is for start worker use celery modul, consume job from rabbitMQ

### --celery_pusher ###
> python -m latihan_hit --celery_pusher

this is for push job to rabbitMQ

### --multy ###
> python -m latihan_hit --multy

this is for start worker use multiprocessing modul