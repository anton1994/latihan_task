import argparse

def main():
    parser = argparse.ArgumentParser(description='NMS Satellite Main Program.')
    parser.add_argument('--celery_worker', action='store_true', help='Execute this program as worker.')
    parser.add_argument('--celery_pusher', action='store_true', help='Execute this program as master.')
    parser.add_argument('--multy', action='store_true', help='Execute this program as master.')
    args = parser.parse_args()

    if args.celery_worker:
        from .celery_worker import start
        start()
        return
    elif args.celery_pusher:
        from .celery_pusher import start_push
        start_push()
        return
    elif args.multy:
        from .multyprosesing_worker import start_multyprocesing
        start_multyprocesing()
        return
    parser.error("Please start this program as worker or master. (--worker or --master)")


