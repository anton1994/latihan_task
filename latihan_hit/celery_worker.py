from __future__ import absolute_import
from celery import Celery
import requests

app = Celery('celery_worker',
             broker='amqp://admin:rahasia@localhost/admin_vhost',
             backend='rpc://')


@app.task
def hit_mutation():
    try:
        req = requests.get('http://127.0.0.1:8070/api/v1/mutation')
        req.raise_for_status()
        if req.status_code == 200:
            print(req.text)
    except:
        print("Api is not Ready")


def start():
    print("masuk")
    log_level = 'INFO'
    worker_process_args = [
        'celery_worker',
        '--loglevel', log_level,
    ]
    app.worker_main(worker_process_args)
