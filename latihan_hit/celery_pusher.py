from .celery_worker import hit_mutation
from apscheduler.schedulers.background import BackgroundScheduler
import time
import signal

kill_now = False

def sigterm_handler(sig, frame):
    global kill_now
    kill_now = True

def start_push():
    app_schedule_obj = BackgroundScheduler()
    app_schedule_obj.add_job(hit_mutation, trigger='interval', seconds=5, replace_existing=True, id='job_id_celery')
    signal.signal(signal.SIGTERM, sigterm_handler)
    app_schedule_obj.start()
    while not kill_now:
        time.sleep(1)

    app_schedule_obj.shutdown()