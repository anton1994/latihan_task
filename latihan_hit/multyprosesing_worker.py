from multiprocessing import Process
import requests
import time


def hitPing():
    try:
        req = requests.get('http://127.0.0.1:8070/api/v1/ping')
        req.raise_for_status()
        if req.status_code == 200:
            print(req.text)
    except:
        print("API is not Ready")


def start_multyprocesing():
    while True:
        try:
            proc = Process(target=hitPing)
            procs = []
            procs.append(proc)
            proc.start()

            for proc in procs:
                proc.join()

            time.sleep(5)
        except (KeyboardInterrupt, SystemExit):
            break
