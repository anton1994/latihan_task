from http import HTTPStatus

from flask_restx import Api
from flask_jwt_extended.exceptions import JWTExtendedException

api = Api(version='1', title='dummy api', doc='/doc/', description='Documentation learning REST API')
