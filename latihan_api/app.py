import logging

from flask import Flask, Blueprint

from .api import api
from .endpoints import all_namespaces

app = Flask(__name__)

gunicorn_error_logger = logging.getLogger('gunicorn.error')
app.logger.handlers.extend(gunicorn_error_logger.handlers)
app.logger.setLevel(logging.DEBUG)

app.config['RESTPLUS_MASK_SWAGGER'] = False
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['APPLICATION_ROOT'] = '/api'
app.config['RESTPLUS_MASK_SWAGGER'] = False

blueprint = Blueprint('rest_api', __name__, url_prefix='/api')
api.init_app(blueprint)
for namespace in all_namespaces:
    api.add_namespace(namespace)
app.register_blueprint(blueprint)
