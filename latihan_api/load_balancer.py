from __future__ import unicode_literals
import gunicorn.app.base


class Gunicorn(gunicorn.app.base.BaseApplication):
    def __init__(self, app):
        self.options = {
            'bind': '0.0.0.0:8070',
            'workers': 1,
            'timeout': 600,
        }
        self.application = app
        super(Gunicorn, self).__init__()

    def load_config(self):
        config = dict([(key, value) for key, value in self.options.items()
                       if key in self.cfg.settings and value is not None])
        for key, value in config.items():
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application
