from .load_balancer import Gunicorn
from .app import app

def main():
    gunicorn = Gunicorn(app)
    gunicorn.run()
