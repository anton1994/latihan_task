from ..api import api
from flask_restx import fields, Resource

ping_namespace = api.namespace('v1/ping', description='Ping.')

_ping_get_return = api.model('ping', {
    'result': fields.String(),
})


@ping_namespace.route('')
class PingAPI(Resource):
    @api.marshal_with(_ping_get_return, description='Ping Pong.')
    def get(self):
        return {"result": "Pong !!!"}
