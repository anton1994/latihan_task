from .ping import ping_namespace
from .mutation import mutation_namespace

all_namespaces = [
    ping_namespace,
    mutation_namespace,
]
