from ..api import api
from flask_restx import fields, Resource
import time
import calendar

mutation_namespace = api.namespace('v1/mutation', description='mutation.')

_mutation_get_return = api.model('mutation', {
    'result': fields.Integer(),
})


@mutation_namespace.route('')
class MutationAPI(Resource):
    @api.marshal_with(_mutation_get_return, description='Mutation Random.')
    def get(self):
        ts = calendar.timegm(time.gmtime())
        return {"result": ts}
